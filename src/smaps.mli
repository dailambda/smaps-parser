type value = Stdint.uint64

module Fields : sig
  type key

  (** Give a key by a string
      in case the key you want to refer to is not in below *)
  val key_of_string : string -> key

  (** Predefined field keys *)
  val size : key
  val kernel_page_size : key
  val mmu_page_size : key
  val rss : key
  val pss : key
  val shared_clean : key
  val shared_dirty : key
  val private_clean : key
  val private_dirty : key
  val referenced : key
  val anonymous : key
  val lazy_free : key
  val anon_huge_pages : key
  val shmem_pmd_mapped : key
  val file_pmd_mapped : key
  val shared_hugetlb : key
  val private_hugetlb : key
  val swap : key
  val swap_pss : key
  val locked : key
end

type t

val get_address : t -> value * value

(** Return an integer in the range 0..31, where each bits represents rwxsp *)
val get_perms : t -> int

val get_offset : t -> value

val get_inode : t -> int

val get_pathname : t -> string

(** Get the size associated with the given key *)
val get_size_exn : t -> Fields.key -> value
val get_size : t -> Fields.key -> value option

val pp_perms : Format.formatter -> int -> unit
val pp_mapping : Format.formatter -> t -> unit
val pp_mappings : Format.formatter -> t list -> unit

(** Read and parse /proc/<pid>/smaps *)
val get_smaps : int -> (t list, string) result Lwt.t

(** Read and parse /proc/self/smaps *)
val get_self_smaps : unit -> (t list, string) result Lwt.t

(** Summalize RSS of all ts *)
val sum_rss : t list -> value

(** Summalize RSS of shared ts *)
val sum_shared_rss : t list -> value
