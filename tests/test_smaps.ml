module RS = Random.State

let test () =
  let open Lwt.Syntax in
  let pid = Unix.getpid () in
  let* s = Smaps.get_smaps pid in
  let* s' = Smaps.get_self_smaps () in
  begin match s, s' with
    | Ok s, Ok _s' ->
        Format.eprintf "%a" Smaps.pp_mappings s;
        assert (List.length s > 0);
        ignore (Smaps.get_size (List.hd s) Smaps.Fields.rss)
    | _ -> assert false
  end;
  Lwt.return_unit

let () =
  let open Alcotest in
  run "test smaps"
    ["smaps", ["test", `Quick, fun () -> Lwt_main.run @@ test () ]]
