# Proc-smaps

An ocaml parser of '/proc/self/smaps'
(See `/proc/[pid]/smaps` section in `man proc` for details).

# Specification
`proc-smaps` parses mappings. A mapping is such as below.

```
00400000-0048a000 r-xp 00000000 fd:03 960637       /bin/bash
Size:                552 kB
KernelPageSize:        4 kB
  :
ProtectionKey:         0
VmFlags: rd ex mr mw me dw
```
A mapping corresponds to `Smaps.t`.
`Smaps.get_smaps <pid>` returns `Smaps.t list Result.t Lwt.t`.
When `mapping : Smaps.t` is derived from this mapping,
`mapping.pathname` is `"/bin/bash"`,
`mapping.perms` is `0b10101` (each bits represent 'rwxsp') and `Smaps.get_size_exn mapping "Size"` is `552 : uint64`.

Device and fields that does not include byte size such as ProtectionKey, VmFlags are not supported now.

# Example
```ocaml
# let smaps = Smaps.get_self_smaps ()
    |> Lwt_main.run
    |> Result.get_ok;;
val smaps : Smaps.t list = ...
# (Stdint.Uint64.to_string @@ Smaps.sum_rss smaps) ^ " KB"
- : string = "19640 KB"
# Smaps.get_pathname (List.hd smaps);;
- : string = "[vdso]"
# List.map (fun x -> Stdint.Uint64.to_int (Smaps.get_size_exn x Smaps.Fields.pss)) smaps;;
- : int list = ...
```
